package com.sjacobpowell.core;

public class Screen extends Bitmap {
	
	public Screen(int width, int height) {
		super(width, height);
	}

	public void render(Game game) {
		clear();
		draw(game.paddle.sprite, game.paddle.getXInt() - game.paddle.sprite.width / 2, game.paddle.getYInt() - game.paddle.sprite.height / 2);
		draw(game.ball.sprite, game.ball.getXInt() - game.ball.sprite.width / 2, game.ball.getYInt() - game.ball.sprite.height / 2);
		game.bricks.forEach(brick -> draw(brick.sprite, brick.getXInt() - brick.sprite.width / 2, brick.getYInt() - brick.sprite.height / 2));
	}
}
	