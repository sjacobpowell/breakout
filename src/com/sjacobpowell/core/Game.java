package com.sjacobpowell.core;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import com.sjacobpowell.entities.Ball;
import com.sjacobpowell.entities.Brick;
import com.sjacobpowell.entities.Paddle;

public class Game {
	private int width;
	private int height;
	public int time;
	public Paddle paddle;
	public Ball ball;
	public List<Brick> bricks;

	public Game(int width, int height) {
		this.width = width;
		this.height = height;
		resetEntities();
	}
	
	private void resetEntities() {
		paddle = new Paddle();
		paddle.reset(width, height);
		
		ball = new Ball();
		ball.reset(width, height);
		
		bricks = new ArrayList<Brick>();
		for(int row = 0; row < 10; row++) {
			for(int column = 0; column < 20; column++) {
				bricks.add(new Brick());
				bricks.get(row * 20 + column).reset(width, height, row, column);
			}
		}
	}
	
	public void tick(boolean[] keys) {
		paddle.xSpeed = keys[KeyEvent.VK_LEFT] && paddle.leftEdge() > 0 ? -5 : 0;
		paddle.xSpeed = keys[KeyEvent.VK_RIGHT] && paddle.rightEdge() < width ? 5 : 0;
		paddle.move();

		ball.xSpeed = ball.x < 0 ? -ball.xSpeed : ball.xSpeed;
		ball.xSpeed = ball.x > width ? -ball.xSpeed : ball.xSpeed;
		ball.ySpeed = ball.y < 0 ? -ball.ySpeed : ball.ySpeed;
		ball.ySpeed = ball.y > height ? -ball.ySpeed : ball.ySpeed;
		bricks.stream().forEach(brick -> {
			if(ball.isColliding(brick)) {
				ball.xSpeed = -ball.xSpeed;
				ball.ySpeed = -ball.ySpeed;
			}
		});
		ball.move();
		time++;
	}
}
