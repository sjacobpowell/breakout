package com.sjacobpowell.entities;

import java.util.Random;

import com.sjacobpowell.core.Bitmap;
import com.sjacobpowell.core.Entity;

public class Brick extends Entity {
	private static final Random random = new Random();
	public Brick() {
		sprite = new Bitmap(32, 16);
		int color = random.nextInt(0xdddddd) + 0x222222;
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = color;
		}
	}
	public void reset(int width, int height, int row, int column) {
		x = (sprite.width / 2) + column * sprite.width;
		y = (sprite.height / 2) + row * sprite.height;
	}

}
