package com.sjacobpowell.entities;

import com.sjacobpowell.core.Bitmap;
import com.sjacobpowell.core.Entity;

public class Paddle extends Entity {
	public Paddle() {
		sprite = new Bitmap(32, 8);
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = 0x0000ff;
		}
	}

	public void reset(int width, int height) {
		x = (width - sprite.width) / 2;
		y = height - sprite.height / 2;
	}

}
