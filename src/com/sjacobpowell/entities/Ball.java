package com.sjacobpowell.entities;

import com.sjacobpowell.core.ArtTools;
import com.sjacobpowell.core.Bitmap;
import com.sjacobpowell.core.Entity;

public class Ball extends Entity {
	public Ball() {
		sprite = new Bitmap(16, 16);
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = 0xff0000;
		}
		sprite = ArtTools.circlefy(sprite, sprite.width / 2.2);
	}

	public void reset(int width, int height) {
		x = (width - sprite.width) / 2;
		y = (height - sprite.height) / 2;
		ySpeed = 2;
		xSpeed = 2;
	}

}
